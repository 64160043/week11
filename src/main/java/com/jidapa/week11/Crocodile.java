package com.jidapa.week11;

public class Crocodile extends Animal implements Swimable,Crawlable,Walkable {

    public Crocodile(String name, int numberOfLeg) {
        super(name, 4);
        
    }

    @Override
    public void crawl() {
        System.out.println(this.toString()+ "crawl." );
        
    }

    @Override
    public void swim() {
        System.out.println(this.toString()+ "swim." );
        
    }

    @Override
    public void eat() {
        System.out.println(this.toString()+ "eat." );
        
    }

    @Override
    public void sleep() {
        System.out.println(this.toString()+ "sleep." );
        
    }

    @Override
    public void walk() {
        System.out.println(this.toString()+ "walk." );
        
    }

    @Override
    public void run() {
        System.out.println(this.toString()+ "run." );
        
    }
    @Override
    public String toString() {
        return "Crocodile(" + this.getName() +")";
    }
    
}
