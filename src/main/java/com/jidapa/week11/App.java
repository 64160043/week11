package com.jidapa.week11;

/**
 * Hello world!
 *
 */
public class App {
    public static void main(String[] args) {
        Bat bat1 = new Bat("Batman");
        bat1.eat();
        bat1.sleep();
        bat1.takeoff();
        bat1.fly();
        bat1.landing();

        Fish fish1 = new Fish("Mane");
        fish1.eat();
        fish1.sleep();
        fish1.swim();

        Plane plane1 = new Plane("Bowing", "Bowing Engine");
        plane1.takeoff();
        plane1.fly();
        plane1.landing();

        Crocodile crocodile1 = new Crocodile("Anatasia", 4);
        crocodile1.crawl();
        crocodile1.eat();
        crocodile1.run();
        crocodile1.sleep();
        crocodile1.swim();

        Submarine submarine1 = new Submarine("Creampie", "Creampie Engine");
        submarine1.swim();

        Rat rat1 = new Rat("Junimo", 4);
        rat1.eat();
        rat1.run();
        rat1.sleep();
        rat1.walk();

        Dog dog1=new Dog("Jame", 4);
        dog1.eat();
        dog1.run();
        dog1.sleep();
        dog1.walk();

        Cat cat1 = new Cat("Sophia", 4);
        cat1.eat();
        cat1.run();
        cat1.sleep();
        cat1.walk();

        Human human1 = new Human("KacDae", 2);
        human1.eat();
        human1.run();
        human1.sleep();
        human1.walk();

        Bird bird1 = new Bird("Shotooo");
        bird1.eat();
        bird1.sleep();
        bird1.takeoff();
        bird1.fly();
        bird1.landing();

        Snake snake1 = new Snake("Amber", 0);
        snake1.crawl();
        snake1.eat();
        snake1.sleep();


        Flyable[] flyablesObjects = { bat1, plane1,bird1 };
        for (int i = 0; i < flyablesObjects.length; i++) {
            flyablesObjects[i].takeoff();
            flyablesObjects[i].fly();
            flyablesObjects[i].landing();

        }

        Swimable[] swimablesobjects = {fish1,crocodile1,submarine1};
        for (int i = 0; i < swimablesobjects .length; i++){
            swimablesobjects[i].swim();
        }

        Crawlable[] crawlablesobjects= {crocodile1,snake1};
        for (int i = 0; i < crawlablesobjects .length; i++){
            crawlablesobjects[i].crawl();
        }

        Walkable[] walkablesobjects = {cat1,crocodile1,dog1,human1,rat1};
        for (int i = 0; i < walkablesobjects .length; i++){
            walkablesobjects[i].run();
            walkablesobjects[i].walk();
        }

       
    }
}
